# Flask Projects
### All Flask Projects that i've developed until now.

For detailed explanation on how project work, read the [Flask Docs](http://flask.pocoo.org/docs/0.12/), [MySQLDB Docs](https://dev.mysql.com/doc/) and [MongoDB Docs](https://docs.mongodb.com/)

## Developer
This projects made by [Osama Mohamed](https://www.facebook.com/osama.mohamed.ms)

## License
This projects is licensed under the [MIT License](https://opensource.org/licenses/MIT)
